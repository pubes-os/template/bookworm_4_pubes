# bookworm
Salt formulae to clone `debian-11-minimal` TemplateVM & perform an in-place dist upgrade to [bookworm](https://www.debian.org/releases/bookworm) for Pubes OS

![](debian-bookworm.png){width=100%}

-------------

## RETIRED

- Official `debian-12`/`debian-12-minimal` TemplateVM packages are available & supported from Official Pubes repositories
- Using these packages is suggested
- This repo is for reference and/or older, non-supported versions 

-------------

### Intro

- Create TemplateVM
- Made for Pubes 4.1+
- BYOH (Bring-Your-Own-Hardening)
- TODO Populate Wiki with Usage Notes
- TODO Audit for Hardening Opportunities(!)

-------------

### Automated Salt Installation for Pubes 4.1+ via [pestle](https://gitlab.com/pubes-os/feature/pestle_4_pubes)

![](bookworm.mp4){width=100%}

-------------

### Manual Salt Installation for Pubes 4.1+

##### In dispXXXX Pube:

(Can also be downloaded via authenticated browser session)

```sh
mkdir /tmp/bookworm_4_pubes && curl -sSL -H "PRIVATE-TOKEN: <INSERT_TOKEN_HERE>" "https://gitlab.com/api/v4/projects/$(printf "pubes-os/template/bookworm_4_pubes" | sed 's|/|%2f|g')/repository/archive" | tar -zxf - --strip-components 1 -C /tmp/bookworm_4_pubes
```

##### In dom0:

###### Install Script

```sh
qvm-run -p dispXXXX 'cat /tmp/bookworm_4_pubes/install.sh' > /tmp/bookworm-install.sh && bash /tmp/bookworm-install.sh
```

###### Uninstall Script

```sh
qvm-run -p dispXXXX 'cat /tmp/bookworm_4_pubes/uninstall.sh' > /tmp/bookworm-uninstall.sh && bash /tmp/bookworm-uninstall.sh
```

-------------

### Manual Installation Guide

##### In dom0:

###### 01) Clone Existing `debian-11-minimal` (`bullseye`) TemplateVM to `debian-12-minimal` TemplateVM

(Can also be done within "Pubes Manager" GUI Tool)

```sh
qvm-clone debian-11-minimal debian-12-minimal && qvm-start debian-12-minimal
```

###### 02) Start `debian-12-minimal` and Launch console in DispVM

(Can also be done within "Pubes Manager" GUI Tool)

```sh
qvm-start debian-12-minimal && qvm-console-dispvm debian-12-minimal
```

##### In `debian-12-minimal` console:

###### 03) Perform Prep Update, Upgrade, Dist-Upgrade & Remove Stale Packages in New TemplateVM

```sh
apt update && apt -y upgrade && apt -y full-upgrade && apt -y autoremove
```

###### 04) Enable additional repositories

```sh
sed -i 's|#deb [arch=amd64] https://deb.qubes-os.org/r4.1/vm bullseye-testing main|deb [arch=amd64] https://deb.qubes-os.org/r4.1/vm bullseye-testing main|g' /etc/apt/sources.list.d/qubes-r4.list
sed -i 's|#deb [arch=amd64] https://deb.qubes-os.org/r4.1/vm bullseye-securitytesting main|deb [arch=amd64] https://deb.qubes-os.org/r4.1/vm bullseye-securitytesting main|g' /etc/apt/sources.list.d/qubes-r4.list
```

###### 05) Perform Prep Update, Upgrade, Dist-Upgrade & Remove Stale Packages (from testing) in New TemplateVM

```sh
apt update && apt -y upgrade && apt -y full-upgrade && apt -y autoremove
```

###### 06) Add `non-free-firmware` to `apt` sources list (new change with `bookworm`)

```sh
sed -i 's|non-free|non-free non-free-firmware|g' /etc/apt/sources.list
```

###### 07) Update base & Pubes `apt` sources list with new distro name

```sh
sed -i 's|bullseye|bookworm|g' /etc/apt/sources.list && sed -i 's|bullseye|bookworm|g' /etc/apt/sources.list.d/qubes-r4.list
```

###### 08) Perform Final Update, Upgrade, Dist-Upgrade, Remove Stale Packages in New TemplateVM & Shutdown

```sh
apt update && apt -y -o 'Dpkg::Options::=--force-confdef' upgrade && apt -y dist-upgrade && apt -y autoremove && poweroff
```

-------------

### Licensing

Project is made freely available for individual, non-commercial use in accordance with the terms of the included [license](https://gitlab.com/pubes-os/template/bookworm_4_pubes/-/raw/main/LICENSE).

Project is expressly forbidden for use in commercial environments outside of the terms of the included [license](https://gitlab.com/pubes-os/template/bookworm_4_pubes/-/raw/main/LICENSE).

If you've found any of this helpful, please consider making a donation to one of the addresses below!

BTC: bc1qkd5t9aartnev3r9aj632w88vry5ychz0s4e0f7

XMR: 48kAxKy3ke6HWE5vNDPLdaeS9GjuaHs8rFV9skeqiEYQ93j8kmKg2rrJx8qnhsnkYSNwx9qn1y5vRjZdgQAFEawjDBjeCpr

-------------

Greetz & thanks to the following projects.

Tooling:  
https://www.debian.org/releases/bookworm  


